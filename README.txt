This is a sandbox project, which contains experimental code for developer use only.

This is a simple module that implements field formats for the datetime field type.

It currently displays the date as an age in years or as a zodiac sign.

In the future this module will support multiple field formats for datetime where there is a need to calculate a value based on one or more date fields, such as:

* Select two date fields in a content type and display time between dates

Eventually this may become a module that implements a new field type that is derived from one or more dates, similar to what Geofield does for locations.