<?php

/**
 * @file
 * A module to display a date as an age or zodiac sign
 *
 * This module provides a date field formatter to display date as age.
 *
 */

/**
 * Implements hook_theme().
 *
 * Defines a function dch_dateage_age (not a template file) that provides the default markup for this module that can be overridden by other modules 
 *
 */
function dch_dateage_theme() {
  return array(
    'dch_dateage_age' => array(
//    'template' => 'dch_dateage_age',    // removed this line so dch_dateage_age function is called to provide theming, instead of using a template file
      'variables' => array(
        'date' => NULL,
      ),
    ),
  );
}


